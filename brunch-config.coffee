module.exports =
  paths:
    watched: ['client']
    public: 'static'
  #ignore cli source
  conventions: ignored: [/^client\/cli/]
  files:
    javascripts: joinTo: 'app.js'
    stylesheets: joinTo: 'app.css'
  npm:
    globals:
      React: 'react'
      ReactDOM: 'react-dom'
      ReactRouter: 'react-router'
      update: 'react-addons-update'
      Redux: 'redux'
      ReactRedux: 'react-redux'
      thunkMiddleware: 'redux-thunk'
      moment: 'moment'
      objectAssign: 'object-assign'
  plugins:
    uglify:
      mangle: false
      compress:
        global_defs: 
          DEBUG: false
