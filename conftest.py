import pytest
import os
from server import create_app
from server.database import db as database
from server.users.models import User
from flask import json


DB_LOCATION = '/tmp/test_app.db'

@pytest.fixture(scope = 'session')
def app():
    app = create_app(config = 'test_settings')
    return app

@pytest.fixture(scope = 'session')
def db(app, request):
    if os.path.exists(DB_LOCATION):
        os.unlink(DB_LOCATION)
    database.app = app
    database.create_all()

    def teardown():
        database.drop_all()
        os.unlink(DB_LOCATION)
    request.addfinalizer(teardown)
    return database

@pytest.fixture(scope = 'function')
def session(db, request):
    session = db.create_scoped_session()
    db.session = session

    def teardown():
        session.remove()

    request.addfinalizer(teardown)
    return session


@pytest.fixture(scope='function')
def user(session, request):
    data = {'email': 'email@ok.ru', 'password': 'pass'}
    user = User(**data)
    session.add(user)
    session.commit()
    
    def teardown():
        user.query.delete()
        session.commit()

    request.addfinalizer(teardown)
    return user

@pytest.fixture(scope='function')
def logged_in_user(user, client, request):
    data = {'email': 'email@ok.ru', 'password': 'pass'}
    response = client.post('/api/v1/users/login', 
                           data=json.dumps(data),
                           content_type = 'application/json')

    return user
