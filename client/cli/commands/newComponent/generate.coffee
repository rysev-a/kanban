translateToConstantName = (word)->
  regex = /[A-Z]{1,}/g
  modified = word.replace regex, (match)-> "_#{match}"
  modified.toUpperCase()

translateToConstantValue = (word)->
  regex = /[A-Z]{1,}/g
  modified = word.replace regex, (match)-> " #{match}"
  modified.toLowerCase()

generateConstantName = (action, componentName)->
  componentName = translateToConstantName(componentName)
  action = translateToConstantName(action)
  "#{action}_#{componentName}"

generateConstantValue = (action, componentName)->
  componentName = translateToConstantValue(componentName)
  action = translateToConstantValue(action)
  "'#{action} #{componentName}'"

generate =
  actions: (actions, componentName) ->
    template = """
    _action: (data)->
        type: constants._constant
        payload: data\n  
    """
    actionsCode = ""
    actions.map (action)->
      constant = generateConstantName(action, componentName)
      code = template
      code = code.replace(/_action/, action)
      code = code.replace(/_constant/, constant)
      actionsCode += code
    actionsCode

  constants: (actions, componentName)->
    constantsCode = ""
    actions.map (action)->
      name = generateConstantName(action, componentName)
      value = generateConstantValue(action, componentName)
      template = """#{name}: #{value}\n  """
      constantsCode += template
    constantsCode

  dispatch: (actions, componentName)->
    template = """_action: (data)-> dispatch(actions._action(data))\n  """
    dispatchCode = ""
    actions.map (action)->
      dispatchCode += template.replace(/_action/g, action)
    dispatchCode


  reducers: (actions, componentName)->
    template = """when constants._constant\n      state\n    """
    reducersCode = ""
    actions.map (action)->
      constant = generateConstantName(action, componentName)
      reducersCode += template.replace(/_constant/g, constant)
    reducersCode


module.exports = generate
