fs = require 'fs'
readline = require 'readline'

generate = require './generate'

command = 
  component:
    name: 'componentName'
    type: 'default'
    templates: []
    actions: []

  getTemplates: ->
    templateFiles = ['index.coffee', 'actions.coffee', 
      'reducers.coffee', 'view.coffee', 'constants.coffee']
    templateFiles.map (templateName)->
      templatePath = "templates/newComponent/#{templateName}"
      cli_path = command.cli_path
      fs.readFile "#{cli_path}/#{templatePath}", 'utf8', (err, file)=>
        command.component.templates.push
          name: templateName
          content: file

  ask:->
    questions = readline.createInterface
      input: process.stdin
      output: process.stdin

    questions.question "input component name:\n", (comopnentName)->
      command.component.name = comopnentName
      questions.question "input component actions:\n", (comopnentActions)->
        command.component.actions = comopnentActions.split(' ')
        command.createComponent()
        questions.close()

  createComponent:->
    command.createFolder()

  createFolder:->
    folderName = "#{command.cli_path}/../app/components/#{command.component.name}"

    fs.mkdir folderName, (e)->
      if !e 
        command.createFiles()
      else
        command.createFiles()
        if e.code is 'EEXIST'
          console.log '\x1b[33m%s\x1b[0m', 
            "already have component #{command.component.name}"
        else
          console.log e

  createFiles:->
    command.parseTemplates()
    command.component.templates.map (template)->
      fileName = "#{command.cli_path}/../app/components/#{command.component.name}/#{template.name}"
      fs.writeFile(fileName, template.content, (e)-> if e then console.log e)

  parseTemplates:->
    actions = command.component.actions

    name = command.component.name
    constantsCode = generate.constants(actions, name)
    actionsCode = generate.actions(actions, name)
    dispatchCode = generate.dispatch(actions, name)
    reducersCode = generate.reducers(actions, name)

    insertData = (template, regexp, data)->
      template.content = template.content.replace(regexp, data)

    command.component.templates.map (template)->
      insertData(template, /\{__component__\}/g, name)
      insertData(template, /\{__componentConstants__\}/g, constantsCode)
      insertData(template, /\{__componentActions__\}/g, actionsCode)
      insertData(template, /\{__componentDispatch__\}/g, dispatchCode)
      insertData(template, /\{__componentReducers__\}/g, reducersCode)


  action: (cli_path)->
    command.cli_path = cli_path
    command.getTemplates()
    command.ask()

module.exports = command
