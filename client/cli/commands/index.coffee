commandsList = ['newComponent']

commands = {}
commandsList.map (commandName)->
  commands[commandName] = require "./#{commandName}"

module.exports = commands
