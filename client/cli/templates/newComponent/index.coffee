actions = require './actions'
view = require './view'

mapStateToProps = (state)->
  {__component__}: state.{__component__}


mapDispatchToProps = (dispatch)->
  {__componentDispatch__}

module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(view)
