[interpretator, cli_path, command, params] = process.argv

commands = require './commands'
messages = require './messages'

if not commands[command]
  console.log messages.unknownCommand(command, commands)
else
  commands[command].action(cli_path.slice(0, -13))
