unknownCommand = (command, commands) ->
  availableCommands = ''
  Object.keys(commands).forEach (cName)-> 
    availableCommands += """- #{cName}\n      """

  message = """
      unknown command \x1b[33m#{command}
        \x1b[0mavailable commands:
            #{availableCommands}
    """

module.exports = {unknownCommand}
