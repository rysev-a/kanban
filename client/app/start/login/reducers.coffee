constants = require '../../constants'

form = (state = {email: '', password: ''}, action)->
  switch action.type
    when constants.UPDATE_LOGIN_FORM
      update(state, 
       "#{action.payload.field}": $set: action.payload.value)
    else
      state

errors = (state = {}, action)->
  switch action.type
    when constants.SUBMIT_LOGIN_FORM
      {}
    when constants.SUBMIT_LOGIN_FORM_SUCCESS
      {}
    when constants.SUBMIT_LOGIN_FORM_ERROR
      action.payload
    when constants.UPDATE_LOGIN_FORM
      update state,
        "#{action.payload.field}": $set: null
    else
      state

module.exports = Redux.combineReducers({form, errors})
