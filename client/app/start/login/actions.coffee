constants = require '../../constants'
API = require '../api'

actions =
  update: (field, value)->
    type: constants.UPDATE_LOGIN_FORM
    payload: {field, value}

  submit: (data)->
    (dispatch)->
      dispatch(type: constants.SUBMIT_LOGIN_FORM)
      API.login(data.form)
        .then (response) -> 
          if response.ok 
            response.json().then (json)->
              dispatch
                type: constants.SUBMIT_LOGIN_FORM_SUCCESS
                payload: json
          else
            response.json().then (json)->
              dispatch
                type: constants.SUBMIT_LOGIN_FORM_ERROR
                payload: json

module.exports = actions
