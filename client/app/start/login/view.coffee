{div, form, input, label, a} = React.DOM

module.exports = ({submit, update, formData})->
  getCssClass = (field)->
    if formData.errors[field]
      'field-error'

  form
    className: 'login-form'
    div className: 'login-form__item',
      input
        className: getCssClass('email')
        placeholder: 'email'
        type: 'email'
        onChange: (e)-> update('email', e.target.value)
    div className: 'login-form__item',
      input
        className: getCssClass('password')
        placeholder: 'password'
        type: 'password'
        onChange: (e)-> update('password', e.target.value)
    a
      className: 'button button-primary'
      onClick: submit.bind(this, formData)
      'Войти'
