[Route, IndexRoute] = [ReactRouter.Route, ReactRouter.IndexRoute].map (component)->
    React.createFactory(component)

module.exports =
  Route path: 'start', component: (require './view'),
    Route path: 'welcome', component: require './welcome'
    Route path: 'login', component: require './login'
    Route path: 'registration', component: require './registration'

