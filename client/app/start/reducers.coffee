login = require './login/reducers'
registration = require './registration/reducers'

module.exports = Redux.combineReducers({
  login, registration
})
