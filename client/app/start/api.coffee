require 'whatwg-fetch'

API_URL = '/api/v1'
API_HEADERS = 
  'Content-Type': 'application/json'

module.exports =
  login: (user)->
    fetch "#{API_URL}/users/login",
      method: 'post'
      headers: API_HEADERS
      credentials: 'include'
      body: JSON.stringify(user)

  register: (user)->
    fetch "#{API_URL}/users/register",
      method: 'post'
      headers: API_HEADERS
      credentials: 'include'
      body: JSON.stringify(user)
