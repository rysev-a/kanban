{div, form, input, label, a} = React.DOM

module.exports = ({submit, update, formData})->
  form
    className: 'registration-form'
    div className: 'registration-form__item',
      input
        placeholder: 'email'
        type: 'email'
        onChange: (e)-> update('email', e.target.value)
    div className: 'registration-form__item',
      input
        placeholder: 'password'
        type: 'password'
        onChange: (e)-> update('password', e.target.value)
    a
      className: 'button button-primary'
      onClick: submit.bind(this, formData)
      'Зарегистрироваться'
