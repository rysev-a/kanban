constants = require '../../constants'

form = (state = {email: '', password: ''}, action)->
  switch action.type
    when constants.UPDATE_REGISTRATION_FORM
      update(state, 
       "#{action.payload.field}": $set: action.payload.value)
    else
      state

errors = (state = [], action)->
  switch action.type
    when constants.ERROR_REGISTRATION_FORM
      action.payload
    else
      state

module.exports = Redux.combineReducers({form, errors})
