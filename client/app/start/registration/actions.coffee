constants = require '../../constants'
API = require '../api'

module.exports =
  update: (field, value)->
    type: constants.UPDATE_REGISTRATION_FORM
    payload: {field, value}

  submit: (data)->
    (dispatch)->
      dispatch(type: constants.SUBMIT_REGISTRATION_FORM)
      API.register(data.form)
        .then (response) -> 
          if response.ok 
            response.json().then (json)->
              dispatch
                type: constants.SUBMIT_REGISTRATION_FORM_SUCCESS
                payload: json
          else
            response.json().then (json)->
              dispatch
                type: constants.SUBMIT_REGISTRATION_FORM_ERROR
                payload: json
