actions = require './actions'
view = require './view'

mapStateToProps = (state)->
  formData: state.start.registration

mapDispatchToProps = (dispatch)->
  update: (field, value)-> dispatch(actions.update(field, value))
  submit: (formData)-> dispatch(actions.submit(formData))

module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(view)


