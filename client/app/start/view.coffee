{div, span, a, p} = React.DOM
Link = React.createFactory(ReactRouter.Link)

class Start extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  checkAuthorization:->
    if @props.currentUser.is_active
      if  @props.location.pathname isnt '/start/welcome'
        @context.router.push('/start/welcome')

    else
      if  @props.location.pathname is '/start/welcome'
        @context.router.push('/start/login')

  componentDidMount:->
    @checkAuthorization()

  componentDidUpdate:->
    @checkAuthorization()

  render:->
    div className: 'start-page',
      if not @props.currentUser.is_active
        div className: 'start-menu',
          Link 
            to: '/start/login'
            className: 'start-menu__item'
            activeClassName: 'active'          
            'Войти'
          span {}, ' / '
          Link 
            to: '/start/registration'
            className: 'start-menu__item'
            activeClassName: 'active'          
            'Зарегистрироваться'      

      @props.children


mapStateToProps = (state)->
  currentUser: state.profile.current

module.exports = ReactRedux.connect(mapStateToProps)(Start)
