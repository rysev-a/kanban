constants = require '../../constants'

defaultState = ->
  is_active: false
  loading: true
  roles: []

module.exports = (state = defaultState(), action)->
  switch action.type
    when constants.SUBMIT_LOGIN_FORM_SUCCESS
      action.payload
    when constants.SUBMIT_REGISTRATION_FORM_SUCCESS
      action.payload
    when constants.SUBMIT_PROFILE_FORM_SUCCESS
      action.payload
    when constants.FETCH_CURRENT_USER_SUCCESS
      action.payload
    when constants.LOGOUT_CURRENT_USER_SUCCESS
      is_active: false
      loading: false
      roles: []
      projects: []
    else
      state
