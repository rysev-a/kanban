constants = require '../../constants'
API = require '../api'

actions =
  fetch: ()->
    (dispatch)->
      dispatch(type: constants.FETCH_CURRENT_USER)
      API.fetch()
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.FETCH_CURRENT_USER_SUCCESS
            payload: json

  logout: ()->
    (dispatch)->
      dispatch(type: constants.LOGOUT_CURRENT_USER)
      API.logout()
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.LOGOUT_CURRENT_USER_SUCCESS
            payload: json

module.exports = actions
