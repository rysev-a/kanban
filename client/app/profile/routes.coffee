[Route, IndexRoute] = [ReactRouter.Route, ReactRouter.IndexRoute].map (component)->
    React.createFactory(component)

module.exports =
  Route path: 'profile', component: (require './view'),
    Route path: 'show', component: require './show'
    Route path: 'edit', component: require './edit'
