{div, form, input, label, a} = React.DOM

class ProfileForm extends React.Component

  @contextTypes:
    router: React.PropTypes.object

  componentDidUpdate:->
    if @props.formData.status.redirect
      @context.router.push('/profile/show')
      @props.reset()

  getCssClass: (field)->
    defaultClassName = 'u-full-width'
    if @props.formData.errors[field]
      defdefaultClassNameault = "#{defaultClassName} field-error"
    defaultClassName

  formElement: (fieldType, fieldName, placeholder, fieldLabel)->
    div className: 'profile-form__item',
      label className: 'profile-form__label', fieldLabel
      input
        value: @props.formData.form[fieldName] or ''
        className: @getCssClass(fieldName)
        placeholder: placeholder
        type: fieldType
        onChange: (e)=> @props.update(fieldName, e.target.value)

  render:->
    form
      className: 'profile-form'
      div className: 'row',
        div className: 'columns six',
          @formElement('text', 'first_name', 'first name', 'Имя')
          @formElement('text', 'last_name', 'last name', 'Фамилия')
        div className: 'columns six',
          @formElement('email', 'email', 'email', 'Электронная почта')
          @formElement('text', 'phone', 'phone', 'Телефон')

      div className: 'profile-form__buttons',
        a
          className: 'button button-primary'
          onClick: @props.submit.bind(this, @props.formData)
          'Сохранить'

module.exports = ProfileForm
