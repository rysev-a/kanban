constants = require '../../constants'

API = require '../api'

actions =
  update: (field, value)->
    type: constants.UPDATE_PROFILE_FORM
    payload: {field, value}

  reset: ->
    type: constants.RESET_PROFILE_FORM

  submit: (data)->
    (dispatch)->
      dispatch(type: constants.SUBMIT_PROFILE_FORM)
      API.update(data.form)
        .then (response) -> 
          if response.ok 
            response.json().then (json)->
              dispatch
                type: constants.SUBMIT_PROFILE_FORM_SUCCESS
                payload: json
          else
            response.json().then (json)->
              dispatch
                type: constants.SUBMIT_PROFILE_FORM_ERROR
                payload: json

module.exports = actions
