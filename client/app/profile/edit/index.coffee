actions = require './actions'
view = require './view'

mapStateToProps = (state)->
  formData: state.profile.edit

mapDispatchToProps = (dispatch)->
  update: (field, value)-> dispatch(actions.update(field, value))
  submit: (formData)-> dispatch(actions.submit(formData))
  reset: -> dispatch(actions.reset())

module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(view)
