constants = require '../../constants'

form = (state = {}, action)->
  switch action.type
    when constants.FETCH_CURRENT_USER_SUCCESS
      {id, first_name, last_name, phone, email} = action.payload
      {id, first_name, last_name, phone, email}

    when constants.SUBMIT_REGISTRATION_FORM_SUCCESS
      {id, first_name, last_name, phone, email} = action.payload
      {id, first_name, last_name, phone, email}

    when constants.SUBMIT_LOGIN_FORM_SUCCESS
      {id, first_name, last_name, phone, email} = action.payload
      {id, first_name, last_name, phone, email}

    when constants.UPDATE_PROFILE_FORM
      update(state, 
       "#{action.payload.field}": $set: action.payload.value)
    else
      state

errors = (state = {}, action)->
  switch action.type
    when constants.SUBMIT_PROFILE_FORM_ERROR
      action.payload
    when constants.UPDATE_PROFILE_FORM
      update state,
        "#{action.payload.field}": $set: null
    else
      state

status = (state = {redirect: false}, action)->
  switch action.type
    when constants.SUBMIT_PROFILE_FORM_SUCCESS
      redirect: true
    when constants.RESET_PROFILE_FORM
      redirect: false
    else
      state

module.exports = Redux.combineReducers({form, errors, status})
