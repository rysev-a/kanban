module.exports =  
  UPDATE_PROFILE_FORM: 'update profile form'
  RESET_PROFILE_FORM: 'reset profile form'
  SUBMIT_PROFILE_FORM: 'submit profile form'
  SUBMIT_PROFILE_FORM_SUCCESS: 'submit profile form success'
  SUBMIT_PROFILE_FORM_ERROR: 'submit profile form error'
