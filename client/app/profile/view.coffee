{div, span, a, p} = React.DOM
Link = React.createFactory(ReactRouter.Link)
actions = require './edit/actions'

class Profile extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  checkAuthorization:->
    if not @props.currentUser.is_active
      @context.router.push('/start/welcome')

  componentDidMount:->
    @checkAuthorization()

  componentDidUpdate:->
    @checkAuthorization()

  render:->
    div className: 'profile-page',
      @props.children

mapStateToProps = (state)->
  currentUser: state.profile.current

mapDispatchToProps = (dispatch)->
  reset: -> dispatch(actions.reset())


module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(Profile)
