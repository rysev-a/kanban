current = require './current/reducers'
edit = require './edit/reducers'


module.exports = Redux.combineReducers({current, edit})
