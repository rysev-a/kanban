require 'whatwg-fetch'

API_URL = '/api/v1'
API_HEADERS = 
  'Content-Type': 'application/json'

module.exports =
  fetch:->
    fetch "#{API_URL}/users/current",
      method: 'get'
      credentials: 'include'

  update: (user)->
    fetch "#{API_URL}/users/#{user.id}",
      method: 'put'
      headers: API_HEADERS
      credentials: 'include'
      body: JSON.stringify(user)

  logout:->
    fetch "#{API_URL}/users/logout",
      method: 'post'
      headers: API_HEADERS
      credentials: 'include'
