Link = React.createFactory(ReactRouter.Link)
{div, span} = React.DOM

view = ({currentUser})->
  profileItem = (fieldName, fieldValue)->
    div className: 'profile-view__item',
      span className: 'profile-view__item-name', fieldName
      span className: 'profile-view__item-value', currentUser[fieldValue]

  div className: 'profile-view',
    profileItem('Имя:', 'first_name')
    profileItem('Фамилия:', 'last_name')
    profileItem('Электронная почта:', 'email')
    profileItem('Телефон:', 'phone')

    div className: 'edit-profile__buttons',
      Link 
        to: '/profile/edit'
        className: 'button button-primary'       
        'Редактировать'

mapStateToProps = (state)->
  currentUser: state.profile.current

module.exports = ReactRedux.connect(mapStateToProps)(view)


