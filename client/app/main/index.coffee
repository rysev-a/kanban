view = require './view'
profileActions = require '../profile/current/actions'
usersActions = require '../users/actions'
projectListActions = require '../projects/list/actions'

mapStateToProps = (state)->
  app: state.app
  currentUser: state.profile.current

mapDispatchToProps = (dispatch)->
  fetchCurrentUser: -> dispatch(profileActions.fetch())
  fetchUserList:-> dispatch(usersActions.fetch())
  fetchProjectList: -> dispatch(projectListActions.fetch())
  logout: -> dispatch(profileActions.logout())

module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(view)
