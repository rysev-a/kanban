{div} = React.DOM
topMenu = require './components/topMenu'
loading = require './components/loading'


class App extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  componentDidMount:->
    @props.fetchCurrentUser()
    @props.fetchUserList()
    @props.fetchProjectList()

  render:->
    div className: 'container',
      loading(app: @props.app)
      if not @props.currentUser.loading
        div className: 'app-content',
          topMenu(currentUser: @props.currentUser, logout: @props.logout)
          @props.children

module.exports = App
