Link = React.createFactory(ReactRouter.Link)
{div, span, a, p} = React.DOM

module.exports = ({currentUser, logout})->
  div
    className: 'top-menu'
    Link 
      to: '/start'
      className: 'top-menu__item'
      activeClassName: 'active'          
      'Главная'

    if currentUser.is_active
      span className: 'user-tabs',
        Link
          to: '/profile'
          className: 'top-menu__item'
          activeClassName: 'active'          
          'Профиль'        
        Link
          to: '/projects'
          className: 'top-menu__item'
          activeClassName: 'active'          
          'Проекты'        
        Link
          to: '/tasks'
          className: 'top-menu__item'
          activeClassName: 'active'          
          'Задачи'
        a
          className: 'top-menu__item'
          onClick: logout
          'Выйти'

