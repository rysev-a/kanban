{div} = React.DOM

module.exports = ({app})->
  cssClass = 'loading-wrapper'
  if app.loading
    cssClass = "#{cssClass} loading"

  div className: cssClass,
    div className: 'loading-img'
