module.exports = 
  projectFilter: (projects, user)->
    if 'admin' in user.roles
      return projects
    
    result =
      data: projects.data.filter (p)-> p.name in user.projects
      status: projects.status

  taskFilter: (tasks, user)->
    if 'admin' in user.roles
      return tasks

    result =
      data: tasks.data.filter (t)-> t.project in user.projects
      status: tasks.status

  projectFilterInTask: (projects, executor)->
    if not executor
      result =
        data: []
        status: projects.status
      return result
    
    result =
      data: projects.data.filter (p)-> 
        (p.users.filter (u)-> u.id is Number(executor)).length
      status: projects.status
