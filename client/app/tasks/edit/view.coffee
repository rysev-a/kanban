Link = React.createFactory(ReactRouter.Link)
{div, h1, a, 
form, label, input, select, option, textarea} = React.DOM

TaskForm = require '../components/form'

class TaskEdit extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  componentWillMount:->
    if @props.taskItem.id isnt Number(@props.params.task_id)
      @props.fetch(@props.params.task_id)
    else
      @props.load(@props.taskItem)

  componentDidUpdate:->
    if @props.taskEdit.redirect
      @context.router.push("/tasks")
      @props.reset()

  render:->
    {taskEdit, users, projects, save, remove, update} = @props

    if users.status.loading
      div {}, '...загрузка пользователей'

    if (@props.params.task_id and
        taskEdit.id isnt Number(@props.params.task_id))
      div {}, '...загрузка задачи'

    else
      div className: 'task-compomnent',
        h1 className: 'page-title', "Изменение задачи #{@props.taskEdit.name}"
        React.createElement TaskForm,        
          users: users
          projects: projects
          task: taskEdit
          save: @props.save
          remove: @props.remove
          update: @props.update

module.exports = TaskEdit
