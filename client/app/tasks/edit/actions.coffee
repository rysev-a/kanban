constants = require '../../constants'
API = require '../api'

module.exports =
  load: (task)->
    type: constants.LOAD_TASK_EDIT
    payload: task
  reset:->
    type: constants.RESET_TASK_EDIT
  update: (field, value)->
    type: constants.UPDATE_TASK_EDIT
    payload: {field, value}
  fetch: (id)->
    (dispatch)->
      dispatch(type: constants.FETCH_TASK)
      API.fetchItem(id)
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.FETCH_TASK_SUCCESS
            payload: json
  save: (task)->
    dispatchType = constants.SAVE_TASK_EDIT_SUCCESS
    (dispatch)->
      dispatch(type: constants.SAVE_TASK_EDIT)
      API.save(task)
        .then((response) => 
          if not response.ok
            dispatchType = constants.SAVE_TASK_EDIT_ERROR
          response.json())
        .then (json) => 
          dispatch
            type: dispatchType
            payload: json

  remove: (id)->
    (dispatch)->
      dispatch(type: constants.REMOVE_TASK_EDIT)
      API.remove(id)
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.REMOVE_TASK_EDIT_SUCCESS
            payload: id
