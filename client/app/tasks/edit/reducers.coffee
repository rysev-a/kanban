constants = require '../../constants'

module.exports = (state = redirect: false, action)->
  switch action.type
    when constants.LOAD_TASK_EDIT
      action.payload
    when constants.UPDATE_TASK_EDIT
      {field, value} = action.payload
      update state,
        "#{field}": $set: value
    when constants.FETCH_TASK_SUCCESS
      action.payload
    when constants.SAVE_TASK_EDIT_SUCCESS
      update state,
        redirect: $set: true
    when constants.RESET_TASK_EDIT
      update state,
        redirect: $set: false
    when constants.REMOVE_TASK_EDIT_SUCCESS
      update state,
        redirect: $set: true
    else
      state
