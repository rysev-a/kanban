module.exports =
  LOAD_TASK_EDIT: 'load task edit'
  UPDATE_TASK_EDIT: 'update task edit'
  RESET_TASK_EDIT: 'reset task edit'
  
  SAVE_TASK_EDIT: 'save task edit'
  SAVE_TASK_EDIT_SUCCESS: 'save task edit success'
  SAVE_TASK_EDIT_ERROR: 'save task edit error'
  
  REMOVE_TASK_EDIT: 'remove task edit'
  REMOVE_TASK_EDIT_SUCCESS: 'remove task edit success'
  REMOVE_TASK_EDIT_ERROR: 'remove task edit error'
