actions = require './actions'
view = require './view'
filters = require 'client/app/filters'

mapStateToProps = (state)->
  taskEdit: state.tasks.edit
  taskItem: state.tasks.item
  users:    state.users
  projects: filters.projectFilterInTask(state.projects.list, state.tasks.edit.executor_id)

mapDispatchToProps = (dispatch)->
  reset: -> dispatch(actions.reset())
  load:   (draft)-> dispatch(actions.load(draft))
  save:   (draft)-> dispatch(actions.save(draft))
  update: (field, value)-> dispatch(actions.update(field, value))
  fetch:  (id)-> dispatch(actions.fetch(id))
  remove: (id)-> dispatch(actions.remove(id))

module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(view)
