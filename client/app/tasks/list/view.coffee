{div, span, a,
table, tbody, thead, tr, th, td} = React.DOM
Link = React.createFactory(ReactRouter.Link)
Filter = require '../components/filter'

class TaskList extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  componentWillMount:->
    if not @props.taskList.status.loadComplete
      @props.fetch()


  taskListTable:->
    div 
      className: 'task-list'
      React.createElement Filter,
        filter: @props.filter
        setFilter: @props.setFilter

      table className: 'task-list-table',
        thead {},
          tr {},
            th {}, '#id'
            th {}, 'Название'
            th {}, 'Описание'
            th {}, 'Статус'
            th {}, 'Исполнитель'

        tbody {}, @tableBody()

  tableBody:->
    if not @props.taskList.data.length
      return tr {}, th {rowSpan: 5}, 'список задач пуст'
    @props.taskList.data.map(@item.bind(this))


  statusMap: (statusID)->
    statuses = [
      'в ожидании'
      'выполняется'
      'завершено'
    ]

    statuses[statusID]

  item: (task)->
    tr
      key: task.id
      td {}, task.id
      td {},
        Link
          to: "/tasks/#{task.id}"
          task.name
      td {}, task.description
      td {}, @statusMap(task.status)
      td {}, task.executor.first_name or task.executor.email

  render:->
    div 
      className: 'task-list'
      div
        className: 'row'
        if @props.taskList.status.loadComplete
          @taskListTable()
        else
          '...загрузка задач'
      div 
        className: 'task-list__buttons row'
        if 'admin' in @props.currentUser.roles or 'manager' in @props.currentUser.roles
          a
            className: 'button button-primary'
            onClick: => @context.router.push("/tasks/create")
            'Создать новую задачу'


module.exports = TaskList
