module.exports =
  FETCH_TASK_LIST: 'fetch task list'
  FETCH_TASK_LIST_ERROR: 'fetch task list error'
  FETCH_TASK_LIST_SUCCESS: 'fetch task list success'

  SET_TASK_LIST_FILTER: 'set task list filter'
  
