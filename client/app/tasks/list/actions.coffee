constants = require '../../constants'
API = require '../api'

actions =
  fetch: ->
    (dispatch)->
      dispatch(type: constants.FETCH_TASK_LIST)
      API.fetchList()
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.FETCH_TASK_LIST_SUCCESS
            payload: json

  setFilter: (filter)->
    (dispatch)->
      dispatch
        type: constants.SET_TASK_LIST_FILTER
        payload: filter

module.exports = actions
