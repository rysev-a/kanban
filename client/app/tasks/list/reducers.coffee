constants = require '../../constants'

data = (state=[], action)->
  switch action.type
    when constants.FETCH_TASK_LIST_SUCCESS
      action.payload
    when constants.SAVE_TASK_EDIT_SUCCESS
      task = action.payload
      index = state.indexOf (state.filter (p)-> p.id is task.id)[0]
      update state, $splice: [[index, 1, task]]
    when constants.SAVE_TASK_CREATE_SUCCESS
      update state, $push: [action.payload]
    when constants.REMOVE_TASK_EDIT_SUCCESS
      id = action.payload
      index = state.indexOf (state.filter (p) -> p.id is id)[0]
      update state, $splice: [[index, 1]]

    when constants.UPDATE_TASK_SUCCESS
      task = action.payload
      index = state.indexOf (state.filter (p)-> p.id is task.id)[0]
      update state, $splice: [[index, 1, task]]
    else
      state

filter = (state='show_all', action)->
  switch action.type
    when constants.SET_TASK_LIST_FILTER
      action.payload
    else
      state

status = (state=loadComplete: false, action)->
  switch action.type
    when constants.FETCH_TASK_LIST_SUCCESS
      loadComplete: true
    else
      state

module.exports = Redux.combineReducers({data, status, filter})
