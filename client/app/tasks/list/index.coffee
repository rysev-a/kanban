actions = require './actions'
view = require './view'
filters = require 'client/app/filters'


statusMap = ['new', 'progress', 'complete']

taskListFilter = (state)->
  tasks = filters.taskFilter(state.tasks.list, state.profile.current)

  data = tasks.data.filter (task)->
    if state.tasks.list.filter is 'show_all'
      return true
    task.status is statusMap.indexOf(state.tasks.list.filter)

  update tasks,
    data: $set: data

mapStateToProps = (state)->
  taskList: taskListFilter(state)
  currentUser: state.profile.current
  filter: state.tasks.list.filter

mapDispatchToProps = (dispatch)->
  fetch: -> dispatch(actions.fetch())
  setFilter: (filter) -> dispatch(actions.setFilter(filter))

module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(view)
