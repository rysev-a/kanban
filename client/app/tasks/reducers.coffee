edit = require './edit/reducers'
item = require './item/reducers'
list = require './list/reducers'
create = require './create/reducers'

module.exports = Redux.combineReducers({
 edit, item, list, create
})
