module.exports =
  save: (task)->
    task =
      name: task.name
      description: task.description
      executor_id: task.executor_id
      deadline_time: task.deadline_time
      project_id: task.project_id
      status: task.status
    JSON.stringify(task)

  create: (task)->
    task =
      name: task.name
      description: task.description
      executor_id: task.executor_id
      manager_id: task.manager_id
      project_id: task.project_id
      deadline_time: task.deadline_time
    JSON.stringify(task)
