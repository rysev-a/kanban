require 'whatwg-fetch'
serializers = require './serializers'

API_URL = '/api/v1'
API_HEADERS = 
  'Content-Type': 'application/json'

module.exports =
  fetchList:->
    fetch "#{API_URL}/tasks",
      method: 'get'

  fetchItem: (id)->
    fetch "#{API_URL}/tasks/#{id}",
      method: 'get'

  save: (item)->
    fetch "#{API_URL}/tasks/#{item.id}",
      method: 'put'
      headers: API_HEADERS
      body: serializers.save(item)

  create: (item)->
    fetch "#{API_URL}/tasks",
      method: 'post'
      headers: API_HEADERS
      body: serializers.create(item)
  
  remove: (id)->
    fetch "#{API_URL}/tasks/#{id}",
      method: 'delete'
      headers: API_HEADERS
