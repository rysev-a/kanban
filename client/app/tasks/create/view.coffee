Link = React.createFactory(ReactRouter.Link)
{div, h1, a, form, label, input, select, textarea, option} = React.DOM

TaskForm = require '../components/form'

class TaskCreate extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  componentDidMount:->
    @props.update('manager_id', @props.currentUser.id)

  componentDidUpdate:->
    if @props.taskCreate.redirect
      @context.router.push("/tasks")
      @props.reset()

  render:->
    if @props.users.status.loading
      return div {}, '... загрузка пользователей'
 
    div className: 'task-compomnent',
      h1 className: 'page-title', "Новая задача"
      div className: 'task-compomnent',
        React.createElement TaskForm,       
          users: @props.users
          projects: @props.projects
          task: @props.taskCreate
          save: @props.save
          remove: @props.remove
          update: @props.update




module.exports = TaskCreate
