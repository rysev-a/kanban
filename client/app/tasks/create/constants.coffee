module.exports =
  UPDATE_TASK_CREATE: 'update task create'
  RESET_TASK_CREATE: 'reset task create'

  SAVE_TASK_CREATE: 'save task create'
  SAVE_TASK_CREATE_SUCCESS: 'save task create success'
  SAVE_TASK_CREATE_ERROR: 'save task create error'
