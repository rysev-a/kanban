constants = require '../../constants'

defaultState =->
  name: "Новая задача #{moment()}"
  description: 'Описание новой задачи'
  redirect: false
  deadline_time: moment().format()

module.exports = (state = defaultState(), action)->
  switch action.type
    when constants.UPDATE_TASK_CREATE
      {field, value} = action.payload
      update state,
        "#{field}": $set: value
    when constants.SAVE_TASK_CREATE_SUCCESS
      update action.payload,
        redirect: $set: true
    when constants.RESET_TASK_CREATE
      defaultState()
    when constants.SUBMI_LOGIN_FORM_SUCCESS
      update state,
        manager_id: $set: action.payload.id
    else
      state
