constants = require '../../constants'
API = require '../api'

module.exports =
  update: (field, value)->
    type: constants.UPDATE_TASK_CREATE
    payload: {field, value}
  reset: ->
    type: constants.RESET_TASK_CREATE
  save: (task)->
    dispatchType = constants.SAVE_TASK_CREATE_SUCCESS

    (dispatch)->
      dispatch(type: constants.SAVE_TASK_CREATE)
      API.create(task)
        .then((response) => 
          if not response.ok
            dispatchType = constants.SAVE_TASK_CREATE_ERROR
          response.json())
        .then (json) =>
          dispatch
            type: dispatchType
            payload: json
