actions = require './actions'
view = require './view'
filters = require 'client/app/filters'

mapStateToProps = (state)->
  taskCreate: state.tasks.create
  users: state.users
  projects: filters.projectFilterInTask(state.projects.list, state.tasks.create.executor_id)
  currentUser: state.profile.current

mapDispatchToProps = (dispatch)->
  reset: -> dispatch(actions.reset())
  save:   (task)-> dispatch(actions.save(task))
  update: (field, value)-> dispatch(actions.update(field, value))

module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(view)
