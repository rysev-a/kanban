{div, span, a, table, tbody, thead, tr, th, td} = React.DOM

monthList = [
  'январь'
  'ферваль'
  'март'
  'апрель'
  'май'
  'июнь'
  'июль'
  'август'
  'сентябрь'
  'октябрь'
  'ноябрь'
  'декабрь'
]

class Calendar extends React.Component
  constructor: (props)->
    super props

    @state = 
      year: @props.selectedDate.year()
      month: @props.selectedDate.month()

  getWeekList:->
    weekList = []
    week = []
    monthDays = @getMonthDates()

    while monthDays.length
      week = []
      [1,2,3,4,5,6,0].map (day)->
        date = monthDays[0] or {day:-> false}
        weekItem = {}
        if date.day() is day
          weekItem = monthDays.shift()
        week.push(weekItem)
      weekList.push(week)

    weekList

  getMonthDates: ->
    {year, month} = @state
    monthDates = []
    date = moment([year, month, 1])
    while date.month() is month
      monthDates.push((moment(date.format())))
      date.add(1, 'days')
    monthDates

  nextMonth:->
    newMonth = @state.month + 1
    if newMonth is 12
      newMonth = 0
      @setState('year': @state.year + 1)
    @setState('month': newMonth)

  prevMonth:->
    newMonth = @state.month - 1
    if newMonth is -1
      newMonth = 11
      @setState('year': @state.year - 1)
    @setState('month': newMonth)

  dateChange: (date, e)->
    if not date.format or date < moment().add(-1, 'day')
      return
    @props.dateChangeHandler(date.format())

  dayCssClass: (date)->
    cssClass = 'calendar-table__date'
    if date.format
      if date > moment().add(-1, 'day')
        cssClass = "#{cssClass} date"

      if date.format('YYYY-MM-DD') is @props.selectedDate.format('YYYY-MM-DD')
        cssClass = "#{cssClass} selected"

    cssClass

  render:->
    div
      className: 'calendar'
      div
        className: 'calendar-title'
        span
          className: 'calendar-title__prev'
          onClick: @prevMonth.bind(this)
        span className: 'calendar-title__month', monthList[@state.month]
        span className: 'calendar-title__year', @state.year
        span
          className: 'calendar-title__next'
          onClick: @nextMonth.bind(this)
      table className: 'calendar-table',
        thead {},
          tr {},
            ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'].map (weekDay, index)->
              th key: index, weekDay
        tbody {},
          @getWeekList().map (week, index)=>
            tr
              className: 'calendar-table__week'
              key: index
              week.map (date, index)=>
                td
                  className: @dayCssClass(date)
                  key: index
                  onClick: @dateChange.bind(this, date)
                  if date.format
                    date.format('D')




module.exports = Calendar
