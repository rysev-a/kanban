{div, a, form, label, input, textarea, select, option} = React.DOM
Calendar = require './calendar'
require('moment/locale/ru')

class TaskForm extends React.Component
  dateChangeHandler: (newDate)->
    @props.update('deadline_time', newDate)

  render:->
    {task, users, projects, update} = @props

    form
      className: 'task-form'
      div className: 'task-form__title', 'Основная информация'
      div className: 'row',
        div className: 'columns six',
          label {}, 'Название'
          input
            className: 'u-full-width'
            type: 'text'
            value: task.name
            onChange: (e)=> 
              update('name', e.target.value)

          label {}, 'Описание'
          textarea
            className: 'u-full-width'
            value: task.description or ''
            onChange: (e)=> 
              update('description', e.target.value)

          label {}, 'Выбрать исполнителя'
          select
            className: 'u-full-width'
            onChange: (e) => update('executor_id', e.target.value)
            value: task.executor_id or 0
            option
              value: 0
              'Выбрать исполнителя'
            users.data.map (user)->
              option
                key: user.id
                value: user.id
                user.first_name or user.email
          
          label {}, 'Выбрать проект'
          select
            className: 'u-full-width'
            onChange: (e) => update('project_id', e.target.value)
            value: task.project_id or 0
            option
              value: 0
              'Выбрать проект'
            projects.data.map (project)->
              option
                key: project.id
                value: project.id
                project.name


        div className: 'columns six',
          label {}, "Выполнить до #{moment(task.deadline_time).format('DD MMMM YYYY')}"
          React.createElement Calendar,
            selectedDate:      moment(task.deadline_time)
            dateChangeHandler: @dateChangeHandler.bind(this)
    
      div className: 'task-comments', 'Комментарии'

      @formButtons()

  formButtons:->
    {task, save, remove} = @props
    div className: 'task__buttons',
      a
        className: 'button button-primary'
        onClick: => save(task)
        'Сохранить'
      a
        className: 'button button-delete'
        onClick: => remove(task.id)
        'Удалить'    

module.exports = TaskForm
