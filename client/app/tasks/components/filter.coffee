{div, span} = React.DOM

class Filter extends React.Component
  constructor: (props)->
    super(props)
    @props = props

  render:->
    filterMap = [
      {title: 'Все задачи',    value: 'show_all'}
      {title: 'Новые',         value: 'new'}
      {title: 'Выполняющиеся', value: 'progress'}
      {title: 'Завершенные',   value: 'complete'}
    ]

    div
      className: 'task-list-filter'
      span className: 'task-list-filter-field', 'Показывать: '
      filterMap.map (filter)=>
        span
          className: @getFilterCss(filter.value)
          key: filter.value
          onClick: @props.setFilter.bind(this, filter.value)
          filter.title

  getFilterCss: (filter)->
    cssClass = 'task-list-filter-item'
    if @props.filter is filter
      cssClass = "#{cssClass} active"
    cssClass

module.exports = Filter
