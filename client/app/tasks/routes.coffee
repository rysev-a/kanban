[Route, IndexRoute] = [ReactRouter.Route, ReactRouter.IndexRoute].map (component)->
    React.createFactory(component)

module.exports =
  Route path: 'tasks', component: (require './view'),
    IndexRoute component: require './list'
    Route path: 'create', components: require './create'
    Route path: ':task_id', components: require './item'
    Route path: ':task_id/edit', components: require './edit'
