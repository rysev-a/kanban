Link = React.createFactory(ReactRouter.Link)
{div, h1, a} = React.DOM

class TaskItem extends React.Component
  componentDidMount:->
    {@task_id} = @props.params
    if @props.taskItem.id isnt Number(@task_id)
      @props.fetch(@task_id)

  render:->
    {taskItem} = @props
    if taskItem.id isnt Number(@props.params.task_id)
      div {}, '...загрузка задачи'

    else
      div className: 'task',
        h1 className: 'page-title', "#{@props.taskItem.name}"
        div
          className: 'task'
          div className: 'task-description', taskItem.description

          @executorButtons()

        if 'admin' in @props.currentUser.roles or 'manager' in @props.currentUser.roles
          div className: 'task__buttons',
            Link
              to: "/tasks/#{taskItem.id}/edit"
              className: 'button button-primary'
              'Редактировать'

  executorButtons:->
    {taskItem, currentUser} = @props
    if ((currentUser.id isnt taskItem.executor_id) or 
         taskItem.status > 1)
      return

    switch taskItem.status
      when 0
        action = @props.start
        title  = 'Начать выполнение'
      when 1
        action = @props.complete
        title  = 'Завершить выполнение'


    div className: 'task__buttons',
      a
        onClick: action.bind(this, taskItem)
        className: 'button button-primary'
        title


module.exports = TaskItem
