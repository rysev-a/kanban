module.exports =
  FETCH_TASK: 'fetch task'
  FETCH_TASK_SUCCESS: 'fetch task success'
  FETCH_TASK_ERROR: 'fetch task error'

  UPDATE_TASK: 'update task'
  UPDATE_TASK_SUCCESS: 'update task success'
  UPDATE_TASK_ERROR: 'update task error'
