constants = require '../../constants'
API = require '../api'

module.exports =
  fetch: (id)->
    (dispatch)->
      dispatch(type: constants.FETCH_TASK)
      API.fetchItem(id)
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.FETCH_TASK_SUCCESS
            payload: json
  start: (task)->
    dispatchType = constants.UPDATE_TASK_SUCCESS
    (dispatch)->
      dispatch(type: constants.UPDATE_TASK)
      task = update task,
        status: $set: 1
      API.save(task)
        .then((response) => 
          if not response.ok
            dispatchType = constants.UPDATE_TASK_ERROR
          response.json())
        .then (json) => 
          dispatch
            type: dispatchType
            payload: json

  complete: (task)->
    dispatchType = constants.UPDATE_TASK_SUCCESS
    (dispatch)->
      dispatch(type: constants.UPDATE_TASK)
      task = update task,
        status: $set: 2
      API.save(task)
        .then((response) => 
          if not response.ok
            dispatchType = constants.UPDATE_TASK_ERROR
          response.json())
        .then (json) => 
          dispatch
            type: dispatchType
            payload: json
