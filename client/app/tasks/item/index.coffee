actions = require './actions'
view = require './view'

mapStateToProps = (state)->
  taskItem: state.tasks.item
  currentUser: state.profile.current

mapDispatchToProps = (dispatch)->
  fetch:    (id)-> dispatch(actions.fetch(id))
  start:    (task)-> dispatch(actions.start(task))
  complete: (task)-> dispatch(actions.complete(task))

module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(view)
