constants = require '../../constants'

module.exports = (state = {}, action)->
  switch action.type
    when constants.FETCH_TASK_SUCCESS
      action.payload
    when constants.SAVE_TASK_EDIT_SUCCESS
      action.payload
    when constants.SAVE_TASK_CREATE_SUCCESS
      action.payload
    when constants.UPDATE_TASK_SUCCESS
      action.payload
    when constants.REMOVE_TASK_EDIT_SUCCESS
      {}
    else
      state

