{div, span, a, p, h1, h2} = React.DOM
Link = React.createFactory(ReactRouter.Link)

class TaskPage extends React.Component
  constructor: ({params})->
    {@task_id} = params

  @contextTypes:
    router: React.PropTypes.object

  checkAuthorization:->
    if not @props.currentUser.is_active
      @context.router.push('/start/welcome')

  componentDidMount:->
    @checkAuthorization()

  componentDidUpdate:->
    @checkAuthorization()

  render:->
    div className: 'tasks',
      @props.children

mapStateToProps = (state)->
  currentUser: state.profile.current

module.exports = ReactRedux.connect(mapStateToProps)(TaskPage)
