app = require './main/reducers'
start = require './start/reducers'
profile = require './profile/reducers'
projects = require './projects/reducers'
tasks = require './tasks/reducers'
users = require './users/reducers'

module.exports = Redux.combineReducers({
  app, 
  start, 
  profile,
  projects,
  tasks,
  users
})
