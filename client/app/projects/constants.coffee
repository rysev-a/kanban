module.exports = objectAssign(
  require './edit/constants'
  require './create/constants'
  require './list/constants'
  require './item/constants'
)
