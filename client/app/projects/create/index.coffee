actions = require './actions'
view = require './view'

mapStateToProps = (state)->
  projectCreate: state.projects.create

mapDispatchToProps = (dispatch)->
  reset: -> dispatch(actions.reset())
  save:   (project)-> dispatch(actions.save(project))
  update: (field, value)-> dispatch(actions.update(field, value))

module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(view)
