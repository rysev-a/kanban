constants = require '../../constants'

defaultState =->
  name: 'Новый проект'
  description: 'Описание нового проекта'
  redirect: false

module.exports = (state = defaultState(), action)->
  switch action.type
    when constants.UPDATE_PROJECT_CREATE
      {field, value} = action.payload
      update state,
        "#{field}": $set: value
    when constants.SAVE_PROJECT_CREATE_SUCCESS
      update action.payload,
        redirect: $set: true
    when constants.RESET_PROJECT_CREATE
      defaultState()
    else
      state
