constants = require '../../constants'
API = require '../api'

module.exports =
  update: (field, value)->
    type: constants.UPDATE_PROJECT_CREATE
    payload: {field, value}
  reset: ->
    type: constants.RESET_PROJECT_CREATE
  save: (project)->
    (dispatch)->
      dispatch(type: constants.SAVE_PROJECT_CREATE)
      API.create(project)
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.SAVE_PROJECT_CREATE_SUCCESS
            payload: json
