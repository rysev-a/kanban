Link = React.createFactory(ReactRouter.Link)
{div, h1, a, form, label, input, select, textarea} = React.DOM

class ProjectCreate extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  componentDidUpdate:->
    if @props.projectCreate.redirect
      @context.router.push("/projects")
      @props.reset()

  render:->
    {projectCreate} = @props
 
    div className: 'project-compomnent',
      h1 className: 'page-title', "Новый проект"
      form
        className: 'project-form'
        div className: 'row',
          div className: 'columns six',
            div className: 'project-form__title', 'Основная информация'
            label {}, 'Название'
            input
              className: 'u-full-width'
              type: 'text'
              value: projectCreate.name
              onChange: (e)=> 
                @props.update('name', e.target.value)

            label {}, 'Описание'
            textarea
              className: 'u-full-width'
              value: projectCreate.description or ''
              onChange: (e)=> 
                @props.update('description', e.target.value)

        div className: 'project__buttons',
          a
            className: 'button button-primary'
            onClick: => @props.save(@props.projectCreate)
            'Сохранить'


module.exports = ProjectCreate
