Link = React.createFactory(ReactRouter.Link)
{div, h1, a} = React.DOM

class ProjectItem extends React.Component
  componentDidMount:->
    {@project_id} = @props.params
    if @props.projectItem.id isnt Number(@project_id)
      @props.fetch(@project_id)

  render:->
    {projectItem} = @props
    if projectItem.id isnt Number(@props.params.project_id)
      div {}, '...загрузка проекта'

    else
      div className: 'project',
        h1 className: 'page-title', "#{@props.projectItem.name}"
        div
          className: 'project'
          div className: 'project-description', projectItem.description

          if projectItem.tasks.length
            div className: 'project-tasks',
              div className: 'project-tasks__title', 'Задачи проекта'
              projectItem.tasks.map (task)->
                div
                  className: 'project-tasks__item'
                  key: task.id
                  "- #{task.name}"

        if 'admin' in @props.currentUser.roles
          div className: 'project-buttons',
            Link
              to: "/projects/#{projectItem.id}/edit"
              className: 'button button-primary'
              'Редактировать'

module.exports = ProjectItem
