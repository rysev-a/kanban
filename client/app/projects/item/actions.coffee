constants = require '../../constants'
API = require '../api'

module.exports =
  fetch: (id)->
    (dispatch)->
      dispatch(type: constants.FETCH_PROJECT)
      API.fetchItem(id)
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.FETCH_PROJECT_SUCCESS
            payload: json  
