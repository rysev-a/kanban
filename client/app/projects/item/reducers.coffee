constants = require '../../constants'

module.exports = (state = {}, action)->
  switch action.type
    when constants.FETCH_PROJECT_SUCCESS
      action.payload
    when constants.SAVE_PROJECT_EDIT_SUCCESS
      action.payload
    when constants.SAVE_PROJECT_CREATE_SUCCESS
      action.payload
    when constants.REMOVE_PROJECT_EDIT_SUCCESS
      {}
    else
      state

