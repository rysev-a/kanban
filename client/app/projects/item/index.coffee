actions = require './actions'
view = require './view'

mapStateToProps = (state)->
  projectItem: state.projects.item
  currentUser: state.profile.current

mapDispatchToProps = (dispatch)->
  fetch: (id)-> dispatch(actions.fetch(id))

module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(view)
