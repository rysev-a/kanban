Link = React.createFactory(ReactRouter.Link)
{div, h1, a, form, label, input, select, textarea} = React.DOM

class ProjectEdit extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  componentWillMount:->
    if @props.projectItem.id isnt Number(@props.params.project_id)
      @props.fetch(@props.params.project_id)
    else
      @props.load(@props.projectItem)

  componentDidUpdate:->
    if @props.projectEdit.redirect
      @context.router.push("/projects")
      @props.reset()

  render:->
    {projectEdit} = @props
 
    if (@props.params.project_id and
        projectEdit.id isnt Number(@props.params.project_id))
      div {}, '...загрузка проекта'

    else
      div className: 'project-compomnent',
        h1 className: 'page-title', "Изменение проекта #{@props.projectEdit.name}"
        form
          className: 'edit-project-form'
          div className: 'row',
            div className: 'columns six',
              div className: 'edit-project-form__title', 'Основная информация'
              label {}, 'Название'
              input
                className: 'u-full-width'
                type: 'text'
                value: projectEdit.name
                onChange: (e)=> 
                  @props.update('name', e.target.value)

              label {}, 'Описание'
              textarea
                className: 'u-full-width'
                value: projectEdit.description or ''
                onChange: (e)=> 
                  @props.update('description', e.target.value)

            div className: 'columns six'

          div className: 'edit-project-buttons',
            a
              className: 'button button-primary'
              onClick: => @props.save(@props.projectEdit)
              'Сохранить'
            a
              className: 'button button-delete'
              onClick: => @props.remove(@props.projectEdit.id)
              'Удалить'

module.exports = ProjectEdit
