module.exports =
  LOAD_PROJECT_EDIT: 'load project edit'
  UPDATE_PROJECT_EDIT: 'update project edit'
  RESET_PROJECT_EDIT: 'reset project edit'
  
  SAVE_PROJECT_EDIT: 'save project edit'
  SAVE_PROJECT_EDIT_SUCCESS: 'save project edit success'
  SAVE_PROJECT_EDIT_ERROR: 'save project edit error'
  
  REMOVE_PROJECT_EDIT: 'remove project edit'
  REMOVE_PROJECT_EDIT_SUCCESS: 'remove project edit success'
  REMOVE_PROJECT_EDIT_ERROR: 'remove project edit error'
