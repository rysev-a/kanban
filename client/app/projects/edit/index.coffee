actions = require './actions'
view = require './view'

mapStateToProps = (state)->
  projectEdit: state.projects.edit
  projectItem: state.projects.item

mapDispatchToProps = (dispatch)->
  reset: -> dispatch(actions.reset())
  load:   (draft)-> dispatch(actions.load(draft))
  save:   (draft)-> dispatch(actions.save(draft))
  update: (field, value)-> dispatch(actions.update(field, value))
  fetch:  (id)-> dispatch(actions.fetch(id))
  remove: (id)-> dispatch(actions.remove(id))

module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(view)
