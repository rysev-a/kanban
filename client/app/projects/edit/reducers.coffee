constants = require '../../constants'

module.exports = (state = redirect: false, action)->
  switch action.type
    when constants.LOAD_PROJECT_EDIT
      action.payload
    when constants.UPDATE_PROJECT_EDIT
      {field, value} = action.payload
      update state,
        "#{field}": $set: value
    when constants.FETCH_PROJECT_SUCCESS
      action.payload
    when constants.SAVE_PROJECT_EDIT_SUCCESS
      update state,
        redirect: $set: true
    when constants.RESET_PROJECT_EDIT
      update state,
        redirect: $set: false
    when constants.REMOVE_PROJECT_EDIT_SUCCESS
      update state,
        redirect: $set: true
    else
      state
