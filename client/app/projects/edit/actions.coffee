constants = require '../../constants'
API = require '../api'

module.exports =
  load: (project)->
    type: constants.LOAD_PROJECT_EDIT
    payload: project
  reset:->
    type: constants.RESET_PROJECT_EDIT
  update: (field, value)->
    type: constants.UPDATE_PROJECT_EDIT
    payload: {field, value}
  fetch: (id)->
    (dispatch)->
      dispatch(type: constants.FETCH_PROJECT)
      API.fetchItem(id)
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.FETCH_PROJECT_SUCCESS
            payload: json
  save: (project)->
    (dispatch)->
      dispatch(type: constants.SAVE_PROJECT_EDIT)
      API.save(project)
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.SAVE_PROJECT_EDIT_SUCCESS
            payload: json
  remove: (id)->
    (dispatch)->
      dispatch(type: constants.REMOVE_PROJECT_EDIT)
      API.remove(id)
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.REMOVE_PROJECT_EDIT_SUCCESS
            payload: id
