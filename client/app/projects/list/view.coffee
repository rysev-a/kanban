{div, a} = React.DOM
Link = React.createFactory(ReactRouter.Link)

class ProjectItem extends React.Component
  render:->
    {project} = @props
    Link
      to: "/projects/#{project.id}"
      className: 'project-list__item columns four'
      div className: 'project-list__item-name', project.name
      div className: 'project-list__item-description', project.description

class ProjectList extends React.Component
  @contextTypes:
    router: React.PropTypes.object

  componentWillMount:->
    if not @props.projectList.status.loadComplete
      @props.fetch()

  list:->
    if @props.projectList.status.loadComplete
      if @props.projectList.data.length
        @props.projectList.data.map (project)->
          React.createElement(ProjectItem, {key: project.id, project})
      else
        'проекты не созданы'

  render:->
    div 
      className: 'project-list'
      div
        className: 'row'
        @list()
      if 'admin' in @props.currentUser.roles
        div 
          className: 'project-list__buttons row'
          a
            className: 'button button-primary'
            onClick: => @context.router.push("/projects/create")
            'Создать новый проект'


module.exports = ProjectList
