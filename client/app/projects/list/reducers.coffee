constants = require '../../constants'

data = (state=[], action)->
  switch action.type
    when constants.FETCH_PROJECT_LIST_SUCCESS
      action.payload
    when constants.SAVE_PROJECT_EDIT_SUCCESS
      project = action.payload
      index = state.indexOf (state.filter (p)-> p.id is project.id)[0]
      update state, $splice: [[index, 1, project]]
    when constants.SAVE_PROJECT_CREATE_SUCCESS
      update state, $push: [action.payload]
    when constants.REMOVE_PROJECT_EDIT_SUCCESS
      id = action.payload
      index = state.indexOf (state.filter (p) -> p.id is id)[0]
      update state, $splice: [[index, 1]]
    else
      state

status = (state=loadComplete: false, action)->
  switch action.type
    when constants.FETCH_PROJECT_LIST_SUCCESS
      loadComplete: true
    else
      state

module.exports = Redux.combineReducers({data, status})
