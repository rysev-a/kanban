constants = require '../../constants'
API = require '../api'

actions =
  fetch: ->
    (dispatch)->
      dispatch(type: constants.FETCH_PROJECT_LIST)
      API.fetchList()
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.FETCH_PROJECT_LIST_SUCCESS
            payload: json  

module.exports = actions
