actions = require './actions'
view = require './view'
filters = require 'client/app/filters'


mapStateToProps = (state)->
  projectList: filters.projectFilter(state.projects.list, state.profile.current)
  currentUser: state.profile.current

mapDispatchToProps = (dispatch)->
  fetch: -> dispatch(actions.fetch())

module.exports = ReactRedux.connect(mapStateToProps, mapDispatchToProps)(view)
