[Route, IndexRoute] = [ReactRouter.Route, ReactRouter.IndexRoute].map (component)->
    React.createFactory(component)

module.exports =
  Route path: 'projects', component: (require './view'),
    IndexRoute component: require './list'
    Route path: 'create', components: require './create'
    Route path: ':project_id', components: require './item'
    Route path: ':project_id/edit', components: require './edit'
