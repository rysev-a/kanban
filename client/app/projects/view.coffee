{div, span, a, p, h1, h2} = React.DOM
Link = React.createFactory(ReactRouter.Link)

class ProjectPage extends React.Component
  constructor: ({params})->
    {@project_id} = params

  @contextTypes:
    router: React.PropTypes.object

  checkAuthorization:->
    if not @props.currentUser.is_active
      @context.router.push('/start/login')

  componentWillMount:->
    @checkAuthorization()

  componentDidUpdate:->
    @checkAuthorization()

  render:->
    div className: 'projects',
      @props.children

mapStateToProps = (state)->
  currentUser: state.profile.current

module.exports = ReactRedux.connect(mapStateToProps)(ProjectPage)
