require 'whatwg-fetch'
serializers = require './serializers'

API_URL = '/api/v1'
API_HEADERS = 
  'Content-Type': 'application/json'

module.exports =
  fetchList:->
    fetch "#{API_URL}/projects",
      method: 'get'

  fetchItem: (id)->
    fetch "#{API_URL}/projects/#{id}",
      method: 'get'

  save: (project)->
    fetch "#{API_URL}/projects/#{project.id}",
      method: 'put'
      headers: API_HEADERS
      body: serializers.save(project)

  create: (project)->
    fetch "#{API_URL}/projects",
      method: 'post'
      headers: API_HEADERS
      body: serializers.create(project)
  
  remove: (id)->
    fetch "#{API_URL}/projects/#{id}",
      method: 'delete'
      headers: API_HEADERS
