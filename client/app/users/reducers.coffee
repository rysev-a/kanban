constants = require '../constants'

data = (state=[], action)->
  switch action.type
    when constants.FETCH_USER_LIST_SUCCESS
      action.payload
    else
      state

status = (state=loading: true, action)->
  switch action.type
    when constants.FETCH_USER_LIST_SUCCESS
      loading: false
    else
      state

module.exports = Redux.combineReducers({data, status})
