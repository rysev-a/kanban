constants = require '../constants'
API = require './api'

actions =
  fetch: ->
    (dispatch)->
      dispatch(type: constants.FETCH_USER_LIST)
      API.fetch()
        .then((response) => response.json())
        .then (json) => 
          dispatch
            type: constants.FETCH_USER_LIST_SUCCESS
            payload: json  

module.exports = actions
