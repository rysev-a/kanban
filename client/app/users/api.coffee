require 'whatwg-fetch'

API_URL = '/api/v1'
API_HEADERS = 
  'Content-Type': 'application/json'

module.exports =
  fetch:->
    fetch "#{API_URL}/users",
      method: 'get'
      credentials: 'include'
