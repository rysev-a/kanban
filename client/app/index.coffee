router = require './router'
reducers = require './reducers'

store = Redux.createStore reducers, 
  Redux.applyMiddleware(thunkMiddleware.default)

provider = ReactRedux.Provider

ReactDOM.render(
  React.createElement(provider, store:store, router)
  document.getElementById('app'))
