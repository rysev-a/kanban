{Router, Route, browserHistory, hashHistory, Redirect, IndexRoute} = ReactRouter

[Route, 
 IndexRoute, 
 Router,
 Redirect] = [Route, 
              IndexRoute, 
              Router,
              Redirect].map (component)->
  component = React.createFactory component

router = Router
  'history': browserHistory
  Redirect from: '/', to: '/start'
  Redirect from: '/start', to: '/start/login'
  Redirect from: '/profile', to: '/profile/show'
  Route path: '/', component: (require './main'),
    require './start/routes'
    require './profile/routes'
    require './projects/routes'
    require './tasks/routes'

  Route path: '*', component: require './error404'

module.exports = router
