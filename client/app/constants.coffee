module.exports = objectAssign(
  require './main/constants'
  require './start/constants'
  require './profile/constants'
  require './projects/constants'
  require './tasks/constants'
  require './users/constants'
)
