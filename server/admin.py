from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView


from .users.models import User, Role
from .projects.models import Project, Task
from .database import db
admin = Admin()

class UserView(ModelView):
    column_exclude_list = ('_password',)


admin.add_view(UserView(User, db.session))
admin.add_view(ModelView(Role, db.session))
admin.add_view(ModelView(Project, db.session))
admin.add_view(ModelView(Task, db.session))

