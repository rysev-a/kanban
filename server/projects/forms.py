from wtforms import StringField
from wtforms.validators import DataRequired
from wtforms_alchemy.validators import Unique
from wtforms_alchemy import ModelForm

from .models import Task

class TaskForm(ModelForm):
    name = StringField('name', validators=[DataRequired(), Unique(Task.name)])
    description = StringField('pasword', validators=[DataRequired()])
