from ..database import db

class Project(db.Model):
    __tablename__ = 'projects'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(length=500), unique=True)
    description = db.Column(db.String(length=5000))
    tasks = db.relationship('Task')

    def __repr__(self):
        return self.name

class Task(db.Model):
    __tablename__ = 'tasks'

    id = db.Column(db.Integer, primary_key=True)
    manager_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    manager = db.relationship('User', foreign_keys=[manager_id])
    executor_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    executor = db.relationship('User', foreign_keys=[executor_id])
    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'))
    project = db.relationship('Project')
    name = db.Column(db.String(length=500), unique=True)
    description = db.Column(db.String(length=5000))
    start_time = db.Column(db.DateTime)
    end_time = db.Column(db.DateTime)
    deadline_time = db.Column(db.DateTime)
    status = db.Column(db.Integer, default=0)

    def __repr__(self):
        return self.name

