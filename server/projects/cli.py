from flask_script import Manager
from .. import db
from .models import Project, Task

project_manager = Manager(usage="Project-based CLI actions")

@project_manager.command
def create_project(project_name):
    
    project = Project(name=project_name,
                description="{} description".format(project_name))
 
    db.session.add(project)
    db.session.commit()

@project_manager.command
def fill_projects():
    for project_name in ['venta', 'brow', 'challenge']:
        create_project(project_name)

# @user_manager.command
# def create_role(role_name):
#     role = Role(name=role_name)
#     db.session.add(role)
#     db.session.commit()

# @user_manager.command
# def fill_roles():
#     for role_name in ['admin', 'manager', 'developer', 'designer', 'tester']:
#         create_role(role_name)
