import time
from flask_restful import Resource, reqparse, fields, marshal
from flask import request, jsonify, json

from ..bcrypt import bcrypt
from ..database import db
from .models import Project, Task
from .forms import TaskForm

user_fields = {
    'id': fields.Integer,
    'email': fields.String,
    'first_name': fields.String
}

task_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'project_id': fields.Integer,
    'project': fields.String,
    'executor_id': fields.Integer,
    'executor': fields.Nested(user_fields),
    'manager': fields.Nested(user_fields),
    'description': fields.String,
    'status': fields.Integer,
    'deadline_time': fields.DateTime(dt_format='iso8601')
}

project_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'description': fields.String,
    'users': fields.List(fields.Nested(user_fields)),
    'tasks': fields.List(fields.Nested(task_fields))
}

def time_sleep(func):
    def decorator(*args, **kwargs):
        import time
        time.sleep(0)
        return func(*args, **kwargs)
    return decorator


class ProjectList(Resource):
    method_decorators = [time_sleep]
    def get(self):
        projects = Project.query.all()
        return marshal(projects, project_fields), 200

    def post(self):
        data = request.json
        project = Project(**data)
        db.session.add(project)
        db.session.commit()

        return marshal(project, project_fields), 201


class ProjectItem(Resource):
    method_decorators = [time_sleep]
    def get(self, project_id):
        project = Project.query.get(project_id)
        if not project:
            return {'message': 'not found'}, 400
        return marshal(project, project_fields), 200

    def put(self, project_id):
        project = Project.query.filter_by(id=project_id)
        project.update(request.json)
        db.session.commit()
        return marshal(project.first(), project_fields), 200

    def delete(self, project_id):
        Project.query.filter_by(id=project_id).delete()
        db.session.commit()
        return {'message': 'delete complete'}, 200


class TaskList(Resource):
    method_decorators = [time_sleep]
    def get(self):
        tasks = Task.query.all()
        return marshal(tasks, task_fields), 200

    def post(self):
        data = request.json
        form = TaskForm(**data)

        if not form.validate():
            response = jsonify(form.errors)
            response.status_code = 400
            return response

        task = Task(**data)
        db.session.add(task)
        db.session.commit()

        return marshal(task, task_fields), 201


class TaskItem(Resource):
    method_decorators = [time_sleep]
    def get(self, task_id):
        task = Task.query.get(task_id)
        if not task:
            return {'message': 'not found'}, 400
        return marshal(task, task_fields), 200

    def put(self, task_id):
        task = Task.query.filter_by(id=task_id)
        task.update(request.json)
        db.session.commit()
        return marshal(task.first(), task_fields), 200

    def delete(self, task_id):
        Task.query.filter_by(id=task_id).delete()
        db.session.commit()
        return {'message': 'delete complete'}, 200
