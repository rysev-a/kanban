from flask import Flask, render_template, json, make_response
from flask_cors import CORS

from .database import db
from .bcrypt import bcrypt
from .authorization import login_manager
from .api import api
from .admin import admin
from .migrate import migrate

def create_app(config = None):
    app = Flask(__name__)
    CORS(app)
    if config is not None:
        app.config.from_object(config)
    app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
    db.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)
    api.init_app(app)
    admin.init_app(app)
    migrate.init_app(app, db)

    return app
