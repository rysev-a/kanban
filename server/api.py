from flask_restful import Api
from .users.resources import (UserList, UserItem, UserLogin,
                              UserLogout, UserCurrent, UserRegister)

from .projects.resources import (ProjectList, ProjectItem, 
                                 TaskList, TaskItem)

api = Api()

# User API
api.add_resource(UserList, '/api/v1/users')
api.add_resource(UserItem, '/api/v1/users/<int:user_id>')
api.add_resource(UserLogin, '/api/v1/users/login')
api.add_resource(UserLogout, '/api/v1/users/logout')
api.add_resource(UserCurrent, '/api/v1/users/current')
api.add_resource(UserRegister, '/api/v1/users/register')

#Project API
api.add_resource(ProjectList, '/api/v1/projects')
api.add_resource(ProjectItem, '/api/v1/projects/<int:project_id>')
api.add_resource(TaskList, '/api/v1/tasks')
api.add_resource(TaskItem, '/api/v1/tasks/<int:task_id>')
