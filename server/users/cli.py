from flask_script import Manager
from .. import db
from .models import User, Role

user_manager = Manager(usage="User-based CLI actions")

@user_manager.command
def create_user(user_name):
    
    user = User(first_name=user_name,
                email="{}@mail.ru".format(user_name),
                password="123")

    db.session.add(user)
    db.session.commit()
    
@user_manager.command
def fill_users():
    for user_name in ['vova', 'alex', 'alina']:
        create_user(user_name)

@user_manager.command
def create_role(role_name):
    role = Role(name=role_name)
    db.session.add(role)
    db.session.commit()

@user_manager.command
def fill_roles():
    for role_name in ['admin', 'manager', 'developer', 'designer', 'tester']:
        create_role(role_name)
