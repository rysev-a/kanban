from server.projects.models import Project
from flask_login import login_user
import json

def test_get_projects_list(session, client):
    for project_name in ['venta', 'brow', 'challenge']:
        new_project = Project(name=project_name, 
                              description="{} description".format(project_name))
        session.add(new_project)
    session.commit()

    response = client.get('/api/v1/projects')
    assert '200' in response.status
    assert len(response.json) > 0

def test_create_new_project(session, client):

    project_data = {'name': 'create project name', 
                    'description': 'project description'}

    response = client.post('/api/v1/projects', 
                            data=json.dumps(project_data), 
                            content_type = 'application/json')

    project = Project.query.filter(Project.name == project_data['name']).one()

    assert (project.description in response.json.get('description') and
            project.id == response.json.get('id'))

def test_get_project(session, client):
    project_data = {'name': 'project name for get test', 
                    'description': 'project description'}

    response = client.post('/api/v1/projects', 
                            data=json.dumps(project_data), 
                            content_type = 'application/json')

    project = Project.query.filter(Project.name == project_data['name']).one()

    response = client.get('/api/v1/projects/{}'.format(project.id))
    assert response.json.get('id') is project.id
    assert response.json.get('description') in project.description

def test_update_user(logged_in_user, client):
    project_data = {'name': 'project name for update test', 
                    'description': 'project description'}

    response = client.post('/api/v1/projects', 
                            data=json.dumps(project_data), 
                            content_type = 'application/json')

    project = Project.query.filter(Project.name == project_data['name']).one()

    update_data = {'name': 'updating project name',
                   'description': 'updating project description'}

    response = client.put('/api/v1/projects/{}'.format(project.id),
                           data=json.dumps(update_data),
                           content_type='application/json')

    response = client.get('/api/v1/projects/{}'.format(project.id))
    assert response.json.get('description') in update_data.get('description')

def test_delete_project(session, client):
    project_data = {'name': 'project name for delete test', 
                    'description': 'project description'}

    response = client.post('/api/v1/projects', 
                            data=json.dumps(project_data), 
                            content_type = 'application/json')

    project = Project.query.filter(Project.name == project_data['name']).one()

    response = client.delete('/api/v1/projects/{}'.format(project.id))
    assert Project.query.filter_by(name=project.name).first() is None

