from server.users.models import User


def test_create_user_instance(session):
    user = User(email='model@mail.ru', password='pass')
    session.add(user)
    session.commit()

    assert user.id is not None
