from flask_script import Manager
from flask_migrate import MigrateCommand
from server import create_app, db

from server.users.cli import user_manager, fill_users
from server.projects.cli import project_manager, fill_projects

manager = Manager(app=create_app('settings'))

@manager.command
def init_db():
    db.create_all()

@manager.command
def fill_db():
    fill_users()
    fill_projects()

if __name__ == '__main__':
    manager.add_command('db', MigrateCommand)
    manager.add_command('users', user_manager)
    manager.add_command('projects', project_manager)
    manager.run()
